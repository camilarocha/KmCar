import { SlidesPage } from './../pages/slides/slides';
import { AddDetailHistoricPage } from './../pages/add-detail-historic/add-detail-historic';
import { AddDetailCarPage } from './../pages/add-detail-car/add-detail-car';
import { Storage } from '@ionic/storage';
import { DataService } from './../providers/data-service';
import { AddConfigPagePage } from './../pages/add-config-page/add-config-page';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

export const fireBaseConfig = {
    apiKey: "AIzaSyDhZ4Imm1ab8y2mz10fjqXvnMmdQix8U0U",
    authDomain: "kmcar-dffdb.firebaseapp.com",
    databaseURL: "https://kmcar-dffdb.firebaseio.com",
    storageBucket: "kmcar-dffdb.appspot.com",
    messagingSenderId: "895694529876"
};

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    AddConfigPagePage,
    AddDetailCarPage,
    AddDetailHistoricPage,
    SlidesPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    AddConfigPagePage,
    AddDetailCarPage,
    AddDetailHistoricPage,
    SlidesPage
  ],
  providers: [DataService, Storage]
})
export class AppModule {}
