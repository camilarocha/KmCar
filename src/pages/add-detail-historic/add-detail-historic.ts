import { AddDetailCarPage } from './../add-detail-car/add-detail-car';
import { Component } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';

/*
  Generated class for the AddDetailHistoric page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add-detail-historic',
  templateUrl: 'add-detail-historic.html'
})
export class AddDetailHistoricPage {

  combustivel: string = '';
  precoTotal: string = '';
  precoLitro: string = '';

  constructor(public navCtrl: NavController, public view: ViewController, public navParams: NavParams) { }

  ionViewDidLoad() {
    let selectedItem = this.navParams.get('selectedItem');

    this.combustivel = selectedItem.combustivel;
    this.precoLitro = selectedItem.precoLitro;
    this.precoTotal = selectedItem.precoTotal;
}

  viewItem(item) {
    this.navCtrl.push(AddDetailCarPage, {
      selectedItem: item
    });
  }
  saveConfig() {
    console.log(this.combustivel);
    if (this.combustivel !== '') {
      let newConfig = {
        combustivel: this.combustivel,
        done: false
      };

      this.view.dismiss(newConfig);
    }
  }

  close() {
    this.view.dismiss();
  }


  back() {
    this.navCtrl.pop();
  }
}