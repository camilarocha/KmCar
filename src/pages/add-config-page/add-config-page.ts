import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';

/*
  Generated class for the AddConfigPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add-config-page',
  templateUrl: 'add-config-page.html'
})
export class AddConfigPagePage {

  unidadeDeMedida: string = '';
  eficiencia: string = '';
  ativarLocalizacao: string = '';
  lembrete: string = '';
  som: string = '';
  constructor(public navCtrl: NavController, public view: ViewController) { }

  ionViewDidLoad() {

  }

  saveConfig() {
    console.log(this.unidadeDeMedida);
    if (this.unidadeDeMedida !== '' && this.eficiencia !== '') {
      let newConfig = {
        unidadeDeMedida: this.unidadeDeMedida,
        eficiencia: this.eficiencia,
        done: false
      };

      this.view.dismiss(newConfig);
    }
  }

  close() {
    this.view.dismiss();
  }

}
