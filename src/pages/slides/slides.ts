import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';

/*
  Generated class for the Slides page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-slides',
  templateUrl: 'slides.html'
})
export class SlidesPage {

  constructor(public navCtrl: NavController, public modalCtrl: ModalController) { }

  ionViewDidLoad() {
    console.log('Hello SlidesPage Page');
  }

  inicializar() {
    let addModal = this.modalCtrl.create(HomePage);

    addModal.present();

  }
}
