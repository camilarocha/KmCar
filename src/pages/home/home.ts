import { SlidesPage } from './../slides/slides';
import { TabsPage } from './../tabs/tabs';
import { AddDetailHistoricPage } from './../add-detail-historic/add-detail-historic';
import { AddDetailCarPage } from './../add-detail-car/add-detail-car';
import { DataService } from './../../providers/data-service';
import { AddConfigPagePage } from './../add-config-page/add-config-page';
import { Component } from '@angular/core';

import { NavController, ModalController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import {
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapsLatLng,
  CameraPosition,
  GoogleMapsMarkerOptions,
  GoogleMapsMarker
} from 'ionic-native';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  items: Array<{
    unidadeDeMedida: string,
    eficiencia: string,
    done: boolean
  }> = [];


  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public dataService: DataService, public toastCtrl: ToastController) {
    this.dataService.getData()
      .then((todos) => {
        if (todos) {
          this.items = JSON.parse(todos);
        }
      });
  }

  ionViewDidLoad() {

  }

  viewHistoric(item) {
    this.navCtrl.push(AddDetailHistoricPage, {
      selectedItem: item
    });
  }

  addConfig() {
    let addModal = this.modalCtrl.create(AddConfigPagePage);

    addModal.onDidDismiss((item) => {
      if (item) {
        this.saveConfig(item);
      }
    });
    addModal.present();
  }

  addDetailCar() {
    let addModal = this.modalCtrl.create(AddDetailCarPage);

    addModal.onDidDismiss((item) => {
      if (item) {
        this.saveConfig(item);
      }
    });
    addModal.present();
  }

  addDetailHistoric() {
    let addModal = this.modalCtrl.create(AddDetailHistoricPage);

    addModal.onDidDismiss((item) => {
      if (item) {
        this.saveConfig(item);
      }
    });
    addModal.present();
  }
  saveConfig(item) {
    this.items.push(item);
    this.dataService.saveData(this.items);
    this.showToast('Configuração adicionada com sucesso!');
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2500
    });

    toast.present();
  }
  ngAfterViewInit() {
    this.loadMap();
  }


  loadMap() {
    let element: HTMLElement = document.getElementById('map');

    let map = new GoogleMap(element);

    // listen to MAP_READY event
    map.one(GoogleMapsEvent.MAP_READY).then(() => console.log('Map is ready!'));

    // create LatLng object
    let ionic: GoogleMapsLatLng = new GoogleMapsLatLng(43.0741904, -89.3809802);

    // create CameraPosition
    let position: CameraPosition = {
      target: ionic,
      zoom: 18,
      tilt: 30
    };

    // move the map's camera to position
    map.moveCamera(position);

    // create new marker
    let markerOptions: GoogleMapsMarkerOptions = {
      position: ionic,
      title: 'Ionic'
    };

    map.addMarker(markerOptions)
      .then((marker: GoogleMapsMarker) => {
        marker.showInfoWindow();
      });
  }

  Slides() {
    this.navCtrl.setRoot(SlidesPage);
  }
}
