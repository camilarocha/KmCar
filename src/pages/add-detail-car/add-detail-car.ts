import { AddDetailHistoricPage } from './../add-detail-historic/add-detail-historic';
import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';

/*
  Generated class for the AddDetailCar page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add-detail-car',
  templateUrl: 'add-detail-car.html'
})
export class AddDetailCarPage {

  combustivel: string = '';
  precoTotal: string = '';
  precoLitro: string = '';

  constructor(public navCtrl: NavController, public view: ViewController) {}

  ionViewDidLoad() {
    
  }

  saveConfig() {
    console.log(this.combustivel);
    if (this.combustivel !== '') {
      let newConfig = {
        combustivel: this.combustivel,
        done: false
      };

      this.view.dismiss(newConfig);
    }
  }

  viewItem(item) {
    this.navCtrl.push(AddDetailHistoricPage, {
      selectedItem: item
    });
  }


  close() {
    this.view.dismiss();
  }
}
